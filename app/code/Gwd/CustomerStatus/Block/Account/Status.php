<?php
namespace Gwd\CustomerStatus\Block\Account;

use Magento\Customer\Model\Session;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Customer\Model\CustomerFactory;

/**
 * Get customer status and form action
 *
 * Class Status
 */
class Status extends Template
{
    /**
     * @var Session
     */
    private $customerSession;

    /**
     * @var CustomerFactory
     */
    private $customerFactory;

    /**
     * Status constructor.
     *
     * @param Context $context
     * @param Session $customerSession
     * @param CustomerFactory $customerFactory
     * @param array $data
     */
    public function __construct(
        Context $context,
        Session $customerSession,
        CustomerFactory $customerFactory,
        array $data = []
    ) {
        $this->customerFactory = $customerFactory;
        $this->customerSession = $customerSession;
        parent::__construct($context, $data);
    }

    /**
     * Get a customer status
     *
     * @return string
     */
    public function getStatus()
    {
        $factory = $this->customerFactory->create();
        $customer = $factory->load($this->customerSession->getCustomer()->getId());

        return $customer->getStatus() ?: '';
    }

    /**
     * Get the form action
     *
     * @return string
     */
    public function getFormAction()
    {
        return $this->getUrl('accountstatus/account/edit');
    }
}
