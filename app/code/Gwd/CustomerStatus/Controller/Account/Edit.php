<?php
namespace Gwd\CustomerStatus\Controller\Account;

use Magento\Customer\Model\Session;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\Controller\ResultInterface;
use Magento\Customer\Model\ResourceModel\CustomerRepositoryFactory;
use Magento\Customer\Model\CustomerFactory;

/**
 * Save or update new customer status
 *
 * Class Edit
 */
class Edit extends Action
{
    /**
     * @var Session
     */
    private $customerSession;

    /**
     * @var CustomerFactory
     */
    private $customerFactory;

    /**
     * Edit constructor.
     * @param Context $context
     * @param CustomerFactory $customerFactory
     * @param Session $customerSession
     */
    public function __construct(
        Context $context,
        CustomerFactory $customerFactory,
        Session $customerSession
    ) {
        $this->customerFactory = $customerFactory;
        $this->customerSession = $customerSession;
        parent::__construct($context);
    }

    /**
     * @return ResponseInterface|Redirect|ResultInterface
     */
    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        $status = $this->getRequest()->getParam('status');
        $factory = $this->customerFactory->create();
        $customer = $factory->load($this->customerSession->getCustomer()->getId());
        $customer->setStatus($status);
        try {
            $customer->save();
            $this->messageManager->addSuccessMessage(__('You saved the status.'));
            return $resultRedirect->setPath('*/*/');
        } catch (\Exception $e) {
            $this->messageManager->addExceptionMessage($e, __('Something went wrong while saving the status.'));
        }

        return $resultRedirect->setPath('*/*/index', ['id' => $this->getRequest()->getParam('id')]);
    }
}
