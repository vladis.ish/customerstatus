<?php

namespace Gwd\CustomerStatus\Observer;

use Exception;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Customer\Model\CustomerFactory;
use Psr\Log\LoggerInterface;

/**
 * Save customer status
 *
 * Class SaveCustomerStatusAfter
 */
class SaveCustomerStatusAfter implements ObserverInterface
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var CustomerFactory
     */
    private $customerFactory;

    /**
     * SaveCustomerStatusAfter constructor.
     *
     * @param LoggerInterface $logger
     * @param CustomerFactory $customerFactory
     */
    public function __construct(
        LoggerInterface $logger,
        CustomerFactory $customerFactory
    ) {
        $this->logger = $logger;
        $this->customerFactory = $customerFactory;
    }

    /**
     * @param Observer $observer
     * @return $this|void
     */
    public function execute(Observer $observer)
    {
        $request = $observer->getEvent()->getDataByKey('request');
        $request = $request->getPostValue();
        $status = $request['customer']['status'];
        $customerId = $request['customer']['entity_id'];
        $factory = $this->customerFactory->create();
        $customer = $factory->load($customerId);
        $customer->setData('status', $status);
        try {
            $customer->save();
        } catch (Exception $e) {
            $this->logger->critical($e->getMessage(), ['exception' => $e]);
        }

        return $this;
    }
}
